const Scrollimages = [
  "./public/screenshot1.png",
  "./public/screenshot2.png",
  "./public/screenshot3.png",
];
document.getElementById("scroll-image").src = "./public/screenshot1.png";
if (history.scrollRestoration) {
  history.scrollRestoration = "manual";
} else {
  window.onbeforeunload = function () {
    window.scrollTo(0, 0);
  };
}

document.getElementById("coworkspace").style.display = "none";
document.getElementById("services").style.display = "none";
document.getElementById("try-image").style.display = "none";
document.getElementById("reviews").style.display = "none";
document.getElementById("footer").style.display = "none";
// document.getElementById("foot1").style.display = "none";
reveal();
function splashScreen() {
  setTimeout(() => {
    document.getElementById("splash-screen").style.display = "none";
    document.getElementById("coworkspace").style.display = "block";
    document.getElementById("services").style.display = "flex";
    document.getElementById("try-image").style.display = "block";
    document.getElementById("reviews").style.display = "flex";
    document.getElementById("footer").style.display = "block";
    // document.getElementById("foot1").style.display = "block";
  }, 3000);
}

function reveal() {
  var reveals = document.querySelectorAll(".reveal");

  for (var i = 0; i < reveals.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals[i].getBoundingClientRect().top;
    var elementVisible = 150;

    if (elementTop < windowHeight - elementVisible) {
      reveals[i].classList.add("active");
    } else {
      reveals[i].classList.remove("active");
    }
  }
}

window.addEventListener("scroll", reveal);
// const button = document.getElementById('rightbutton');
function rightArrow() {
  const container = document.getElementById("scroll-container");
  sideScroll(container, "right", 25, 100, 10);
}

function leftArrow() {
  const container = document.getElementById("scroll-container");
  sideScroll(container, "left", 25, 100, 10);
}

function sideScroll(element, direction, speed, distance, step) {
  scrollAmount = 0;
  var slideTimer = setInterval(function () {
    if (direction == "left") {
      element.scrollLeft -= step;
    } else {
      element.scrollLeft += step;
    }
    scrollAmount += step;
    if (scrollAmount >= distance) {
      window.clearInterval(slideTimer);
    }
  }, speed);
}

function scrollInto(id) {
  console.log(id);
  var elmnt = document.getElementById(id);
  elmnt.scrollIntoView({
    behavior: "smooth",
    block: "start",
    inline: "nearest",
  });
}

var count = 0;

function rightArrow() {
  if (count != 2) {
    count = count + 1;
    document.getElementById("scroll-image").src = Scrollimages[count];
  }
  if (count < 2) {
    document.getElementById("right-arrow").style.opacity = "100%";
    document.getElementById("left-arrow").style.opacity = "50%";
  } else {
    document.getElementById("right-arrow").style.opacity = "50%";
    document.getElementById("left-arrow").style.opacity = "100%";
  }
}
function leftArrows() {
  console.log(count);
  if (count != 0) {
    count = count - 1;
    document.getElementById("scroll-image").src = Scrollimages[count];
  }
  if (count > 0) {
    document.getElementById("left-arrow").style.opacity = "100%";
    document.getElementById("right-arrow").style.opacity = "50%";
  } else {
    document.getElementById("left-arrow").style.opacity = "50%";
    document.getElementById("right-arrow").style.opacity = "100%";
  }
}
